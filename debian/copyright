Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: CAD Exchanger SDK
Upstream-Contact: CAD Exchanger <info@cadexchanger.com>
Source: https://cadexchanger.com/products/sdk/

Files: debian/*
Copyright: 2022, Neil Roza <neil@rtr.ai>
License: BSD-2-Clause

Files: *
Copyright: 2022, CAD Exchanger <info@cadexchanger.com>
License: EULA
 Document version: 3.4
 Last updated: August 17, 2020
 This CAD Exchanger License Agreement ("Agreement") is a legal agreement between
 CADEX Ltd or any of its affiliates ("Licensor") and you (either an individual
 or a legal entity) ("Licensee") for the Licensed Software (as defined below).
 Collectively, Licensor and Licensee referred to hereinafter as the "Parties" or
 singularly the "Party", agree as follows:
 ===============================================================================
 1. DEFINITIONS
 * "Affiliate" of a Party shall mean an entity (i) which is directly or
   indirectly controlling such Party; (ii) which is under the same direct or
   indirect ownership or control as such Party; or (iii) which is directly
   or indirectly owned or controlled by such Party.
 * "Application" shall be mean Licensee's software created using the
   Licensed Software.
 * "Contractor(s)" shall mean third party consultants, distributors and
   contractors performing services to the Licensee under applicable
   contractual arrangement.
 * "License Term" shall mean the validity period of the license, during
   which the Licensee is entitled to use the Licensed Software. Unless
   otherwise defined in this Agreement or explicitly agreed between the
   Parties, the License Term is one (1) year from the date of providing a
   license key by Licensor.
 * "Licensed Software" shall mean software, documentation, example programs,
   license keys and other materials, including any updates thereto, that are
   provided by Licensor to Licensee under this Agreement.
 * "Redistributables" shall mean the portions of the Licensed Software that
   may be distributed pursuant to the terms of this Agreement in object code
   form only and as described in the file redist.txt included into the
   Licensed Software.
 * "Renewal Term" shall mean an extension of previous License Term.
 * "Updates" shall mean releases of the Licensed Software containing
   enhancements, new features, corrections and other modifications.
 2. LICENSES GRANTED
 2.1. CAD Exchanger GUI
 License granted for CAD Exchanger GUI is defined in Schedule A.
 2.2. CAD Exchanger Cloud
 License granted for CAD Exchanger Cloud is defined in Schedule B.
 2.3. CAD Exchanger Development Tools
 CAD Exchanger Development Tools include:
 * CAD Exchanger SDK and add-ons thereto
 * CAD Exchanger CLI
 * CAD Exchanger Web Toolkit
 * CAD Exchanger Cloud API
 * Other CAD Exchanger development tools and add-ons
 License granted for CAD Exchanger development tools is defined in Schedule C.
 3. LICENSE RESTRICTIONS
 3.1. Common Restrictions
 Licensee may not:
 * remove or alter any copyright, trademark or other proprietary rights
   notice contained in any portion of the Licensed Software;
 * reverse engineer, decompile or disassemble the Licensed Software;
 * attempt to modify or tamper with the normal function of a license manager
   that regulates usage of the Licensed Software;
 * distribute, sublicense, rent, lease or otherwise use the Licensed
   Software except as provided in this Agreement.
 Licensee shall cause all of its Affiliates and Contractors entitled to use
 licenses granted under this Agreement, to be contractually bound to comply with
 the relevant terms of this Agreement and not to use the Licensed Software
 beyond the terms hereof and for any purposes other than operating within the
 scope of their services for Licensee. Licensee shall be responsible for any and
 all actions and omissions of its Affiliates and Contractors relating to the
 Licensed Software and use thereof (including but not limited to payment of all
 applicable fees).
 3.2. Evaluation License
 When using the Licensed Software under Evaluation license, the Licensee may use
 Licensed Software only for internal evaluation purposes and only for the term
 of the evaluation time period. Licensee may not distribute any portion of the
 Licensed Software. When Licensed Software is used for developing the
 Application, the Application may only be used for evaluation purposes and only
 for the term of the evaluation period.
 4. UPDATES
 4.1. Access to Updates
 Depending on the purchased license type, Licensee may be entitled to receive
 Updates during the License Term. Following that period, Licensor shall no
 longer make the Updates available to Licensee unless Licensee pays license fees
 to retain access to Updates, for the new License Term, at terms and conditions
 applicable at the time of renewal.
 4.2. Single License Term
 In the case of distinct purchases of components of the Licensed Software, all
 such components shall have one common License Term.
 If Licensee purchases additional component(s) of the Licensed Software within
 three months after initial purchase (or commencement of a new Renewal Term)
 then the License Term of the additional component(s) is set equal to initial
 License Term.
 If Licensee purchases additional component(s) later than three (3) months after
 initial purchase (or commencement of a new Renewal Term) then the License Term
 will be set to the License Term of the additional component(s). At the same
 time Licensee shall pay a prorated fee for extending the initial License Term
 for initial component(s).
 5. SUPPORT, CONSULTING AND OTHER SERVICES
 No support, consulting or any other services shall be delivered by Licensor
 under this agreement. Any service shall be subject to a separate agreement
 between Licensor and Licensee.
 6. PAYMENTS
 6.1. License Fees
 The license fees shall not be refunded or claimed as a credit in any event or
 for any reason whatsoever.
 6.2. Tier-Based License Fees
 For some Licensed Software, license fees may be defined in accordance with the
 revenue received by the Licensee in certain period. In this case the revenue
 shall mean total gross revenue generated by:
 * Application which uses Licensed Software (plus any other Licensees
   software that may benefit from results of Licensed Software functioning)
   and/or;
 * Services rendered by or any other activities by Licensee or its
   Affiliates involving use of Licensed Software.
 In event Application supports functioning of hardware distributed by Licensee
 combined gross revenue of hardware and Application shall be taken into account.
 If Application represents a distinct plug-in, module or another similar add-on
 then the total gross revenue of the Licensees software (plus hardware if
 applicable), which uses that add-on shall be taken into account.
 If during the current License Term, the revenue exceeds the threshold defined
 for the current tier then the conditions for the new tier shall apply as of
 next License Term. In addition, whenever applicable, a one-time upgrade fee
 (defined as a difference between license fees for the two tiers) shall be due.
 6.3. Payment Terms
 License Fees and any other charges under this Agreement shall be paid by
 Licensee no later than ten (10) days from the date of the applicable invoice
 from Licensor unless otherwise agreed between the parties.
 In the case when payment is delayed for more than 10 days after commencement of
 the new Renewal Term, a late payment charge of one percent per month shall be
 charged on any unpaid balances that remain past due. An incomplete month shall
 be rounded up.
 6.4. Reinstatement fees
 When Licensee decides to suspend access to Updates (whenever permitted by the
 Agreement) and later decides to renew such access, the reinstatement fee shall
 be applied. The reinstatement fee will be calculated as a minimum of: a) 80%
 (eighty percent) of the respective fees which would have been paid by Licensee
 if the access had not been suspended; b) cost of a new license at the time of
 reinstatement.
 6.5. Taxes
 All License Fees and other charges are exclusive of any value added tax, use
 tax, sales tax, withholding tax and other taxes or duties (Taxes) levied
 directly for the sale, delivery or use of Licensed Software hereunder pursuant
 to any applicable law. Such applicable Taxes shall be paid by Licensee to
 Licensor.
 7. COMPLIANCE; AUDIT RIGHTS
 7.1. License compliance verification
 To ensure compliance with the Agreement, the license protection mechanism
 included into Licensed Software may periodically perform automatic checks on
 any device where Licensed Software is used.
 If the Application is detected to be used with a license key beyond the period
 of the corresponding License Term the license protection mechanism may
 automatically collect information and transmit it to Licensor for license
 compliance verification. Such information may include license key, Licensed
 Software version, IP address and other impersonal data.
 7.2. Licensors Audit Rights
 To ensure compliance with the Agreement, the Licensor or its authorized
 representative, may conduct audit of the Licensee with respect to the
 Licensees use of the Licensed Software. Within 5 (five) business days from
 the date of the request, the Licensee shall provide all pertinent records and
 information requested in order to verify compliance with the Agreement along
 with a signed verification that all such information is complete and correct.
 Such records and information may include:
 * Financial statements, if Licensed Software is provided under tier-based
   licenses;
 * List of software products using Licensed Software or results received
   therewith;
 * Any other information Licensor may reasonably request.
 If the audit reveals that Licensee is using the Licensed Software beyond scope
 of the licenses Licensee has paid for, Licensee shall pay the Licensor 150% of
 any amounts owed for such unauthorized use plus cost of conducting such audit
 within thirty (30) days from receipt of the corresponding invoice from
 Licensor.
 8. CONFIDENTIALITY
 Each Party acknowledges that during the term of this Agreement each Party may
 receive information from the other Party, that is confidential and of great
 value to the other Party, and the value of which would be significantly reduced
 if disclosed to third parties (Confidential Information). Accordingly,
 when a Party (the Receiving Party) receives Confidential Information from
 the other Party (the Disclosing Party), the Receiving Party shall only
 disclose such information to its employees, employees of its Affiliates,
 directors, officers, attorneys, and Contractors on a need to know basis, and
 shall cause its employees and employees of its Affiliates to:
 * maintain any and all Confidential Information in confidence;
 * not disclose the Confidential Information to a third party without the
   Disclosing Party's prior written approval; and
 * not, directly or indirectly, use the Confidential Information for any
   purpose other than for exercising its rights and fulfilling its
   responsibilities pursuant to this Agreement.
 Each Party shall take reasonable measures to protect the Confidential
 Information of the other Party, which measures shall not be less than the
 measures taken by such Party to protect its own confidential and proprietary
 information.
 Obligation of confidentiality shall not apply to information that
 * is or becomes publicly known through no action or inaction of the
   Receiving Party;
 * was already in the possession of the Receiving Party at the time of
   disclosure without an obligation of confidentiality, direct or indirect,
   to the Disclosing Party;
 * is obtained by the Receiving Party from an independent third party
   without a breach of such third partys obligations of confidentiality;
 * is independently developed by the receiving Party without use of or
   reference to materials provided by the disclosing Party;
 * the Receiving Party is legally compelled to disclose, in which case the
   Receiving Party shall notify the Disclosing Party of such compelled
   disclosure and assert the privileged and confidential nature of the
   information and cooperate fully with the Disclosing Party to limit the
   scope of disclosure and the dissemination of disclosed Confidential
   Information to the minimum extent necessary.
 The obligations under this Section 8 shall continue to remain in force for a
 period of five (5) years after the last disclosure, and, with respect to trade
 secrets, for so long as such trade secrets are protected under applicable trade
 secret laws.
 8.1. Confidential files
 The Licensor may use computer files it received from Licensee to debug or
 otherwise improve Licensed Software. Licensor shall have the right to extract
 and to store a minimum possible subset of information from the received files
 for its internal testing purposes.
 9. TERM AND TERMINATION
 9.1. Agreement Term
 This Agreement shall enter into force upon any of the following:
 * explicit signature by both Parties;
 * payment of any of the invoice from Licensor;
 * using click-to-accept or similar mechanism;
 * any other due acceptance by both Parties.
 The Agreement shall remain in force for as long as there is any license
 purchased under this Agreement, unless and until terminated pursuant to the
 terms of this Section 9.
 9.2. Expiration
 This Agreement shall be deemed expired upon expiration of the License Term when
 license protection mechanism included into Licensed Software limits its
 functioning to that License Term. In all other cases, unless expressly agreed
 in writing between the Parties, the Agreement shall remain in force until its
 termination.
 9.3. Termination and suspension of rights
 9.3.1. Termination by Licensee
 Licensee may terminate this Agreement upon thirty (30) days prior written
 notice to Licensor of Licensees decision to terminate provided that such
 termination will not relieve Licensee of any duties and obligations incurred
 prior to the effective date of termination.
 9.3.2. Termination by Licensor
 Licensor may terminate this Agreement should Licensee violate or is reasonably
 suspected to violate any provision of this Agreement, or fail to pay any due
 fees within thirty (30) days of the invoice date, and further fail to remedy
 such nonperformance, noncompliance or nonpayment within ten (10) days following
 written notice from Licensor.
 Instead of termination, Licensor shall have the right to suspend or withhold
 grants of all rights to the Licensed Software hereunder, including but not
 limited to the licenses and support.
 9.4. Parties Rights and Duties upon Termination
 Upon expiry or termination of the Agreement, Licensee shall cease using the
 Licensed Software and distribution of the Redistributables under this
 Agreement.
 Upon termination the Licensee shall destroy all copies of the Licensed Software
 and all related materials and will certify the same to Licensor upon its
 request.
 Expiry or termination of this Agreement for any reason whatsoever shall not
 relieve Licensee of its obligation to pay any fees accrued or payable to
 Licensor prior to the effective date of termination, and Licensee shall pay to
 Licensor all such fees within five (5) business days upon the effective date of
 termination.
 10. NO WARRANTY
 THE MATERIALS ARE PROVIDED "AS IS" WITH NO WARRANTY OF ANY KIND, EITHER
 EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE WARRANTY OF DESIGN,
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OF
 INTELLECTUAL PROPERTY RIGHTS.
 11. LIMITED LIABILITY
 If, warranty disclaimer notwithstanding, Licensor is held liable to Licensee,
 Licensor's entire liability to Licensee shall be limited, at Licensors
 discretion, to correction of error in the Licensed Software, replacement of the
 Licensed Software or return of the applicable fees paid for defective
 Components prorated to the time period during which the Licensee is not able to
 use the Licensed Software under the terms of this Agreement.
 Licensor shall not, under any circumstances, be liable for any damages, damages
 for loss of profits or interruption of business or for loss or corruption of
 data.
 Licensee shall indemnify and hold Licensor, its Affiliates, Contractors and
 suppliers, harmless from and against any claims or liabilities arising from of
 the use of Licensed Software and/or Application.
 12. GENERAL PROVISIONS
 Licensed Software is licensed, not sold. Licensor shall own title, intellectual
 property and any other rights not expressly granted to Licensee under this
 Agreement.
 Licensee shall not be entitled to assign or transfer all or any of its rights
 and obligations under this Agreement without the prior written consent of
 Licensor. Licensor shall be entitled to freely assign or transfer any of its
 rights or obligations under this Agreement.
 Licensor may include Licensee's company name and logo into a list of its
 customers and in its public communications.
 12.1. Surviving Sections
 The provisions of Sections 1, 8, 9.4, 10, 11, 12 shall survive the expiration
 or termination of this Agreement.
 12.2. Entire Agreement
 This Agreement and the exhibits hereto, constitute the complete agreement
 between the Parties and supersede all prior discussions, representations, and
 proposals, written or oral, with respect to the subject matters discussed
 herein.
 In the event of any conflict or inconsistency between this Agreement and any
 Purchase Order, the terms of this Agreement will prevail over the terms of the
 Purchase Order with respect to such conflict or inconsistency.
 12.3. Modifications
 From time to time Licensor may change the terms of this Agreement by publishing
 them on its public web-site and/or including into Licensed Software. Each new
 version of the Agreement shall become effective upon such publication and shall
 supersede previous versions thereof.
 13. THIRD PARTY SOFTWARE
 The Licensed Software may include third party software and materials. The
 license terms with those software and materials apply to Licensee's use of
 them, and Licensor is not liable for them.
 14. Schedule A. Additional terms for CAD Exchanger GUI
 Licensor grants to Licensee a worldwide, non-exclusive, non-transferable
 license, valid for the License Term, to use the Licensed Software in accordance
 with its purpose and as documented in the Licensed Software.
 The Licensed Software can be installed and used in accordance with one of the
 usage models described below
 14.1. Single user license
 When using the Licensed Software under the Single user license, Licensee may
 install and use the Licensed Software only on a single designated computer by a
 single designated user the license has been activated by, unless otherwise
 agreed between the Parties.
 The Licensee shall have the right to reassign the license to another designated
 computer once during the License Term. The license for the previous designated
 computer shall be simultaneously revoked.
 14.2. Server license
 When using the Licensed Software under the Server license, Licensee may install
 and use the Licensed Software only on a single designated computer by no more
 than the authorized number of concurrent users. A separate license is required
 for each additional concurrent user and/or computer in all other cases.
 The Licensee shall have the right to reassign the license to another designated
 computer once during the License Term. The license for the previous designated
 computer shall be simultaneously revoked.
 14.3. Site license
 When using the Licensed Software under the Site license, Licensee may install
 and use the Licensed Software on a an unlimited number of designated computers
 in the authorized companys office with registered physical address, grouped
 into one common network domain. The Licensed Software may be used by no more
 than the authorized number of concurrent users. The Licensed Software may only
 be used by employees of the authorized office. A separate license is required
 for each additional use.
 15. Schedule B. Additional terms for CAD Exchanger Cloud
 Licensor grants to Licensee a worldwide, non-exclusive, non-transferable single
 user license, valid for the License Term, to use the Licensed Software as
 online service accessible via public cloud to upload, view, analyze, download,
 convert, share and concurrently access 3D data.
 16. Schedule C. Additional terms for CAD Exchanger Development Tools
 16.1. Licensing Models
 CAD Exchanger Development Tools can be licensed using one of the licensing
 models, described below. Any other usage must be pre-approved by Licensor.
 License protection mechanism may exploit technical means to enforce and verify
 compliance to selected model.
 16.1.1. Application Distribution
 When using the Licensed Software under the Application Distribution
 licensing model, the following conditions shall be respected:
 * Application shall be a self-contained binary application (typically
   running on desktop platforms) for end-users.
 * Application shall be distributed for a fee.
 * No usage in online, cloud, web or other similar applications is
   permitted.
 16.1.2. Server Deployment
 When using the Licensed Software under the Server Deployment licensing
 model, the Application shall be deployed on a server, accessible by single or
 multiple users (e.g. Software as a Service), in the cloud or on-premise.
 16.1.3. In-house Usage
 When using the Licensed Software licensed for in-house usage, the Application
 may only be used by Licensees employees, employees of Affiliates and/or
 Contractors. In-house usage shall not be used in any revenue generating
 activities.
 16.2. Development with Licensed Software
 Licensor grants to Licensee a worldwide, non-exclusive, non-transferable
 license, valid for the License Term, to use and copy the Licensed Software for
 the sole purposes of designing, developing, and testing Application that
 conforms to one of the licensing models defined in this Section.
 The Application name shall be provided to Licensor and may be included into the
 license key. Use of Licensed Software for and with other Application(s)
 requires additional Agreement(s) with Licensor.
 Upon expiry of the initial License Term, the respective License Terms shall be
 automatically extended to one or more Renewal Term(s), unless and until either
 Party notifies the other Party in writing that it does not wish to continue the
 License Term. Such notification shall be provided to the other Party no less
 than thirty (30) days before expiry of the respective License Term.
 Any such Renewal Term shall be subject to license fees applicable at the
 commencement date of any such Renewal Term.
 In the event of unpaid license fees within ten (10) days after commencement of
 the Renewal Term Licensor may execute its rights on terminating the Agreement
 as defined in Section 9.
 16.2.1. License key
 The license key shall be provided by Licensor within five (5) business days
 upon payment receipt of the due license fees.
 With every new Renewal Term Licensor shall provide an updated license key which
 must be used by Licensee to replace the previous license key in order to
 continue using Licensed Software.
 16.3. Distribution of Application
 Licensor grants to Licensee a worldwide, non-exclusive, non-transferable,
 revocable (for cause pursuant to this Agreement) license, valid for the License
 Term, to distribute the object code form of Redistributables for execution of
 Application that conforms to one of the licensing models defined in this
 Section.
 Copies of Redistributables may only be distributed with and for the sole
 purpose of executing Application permitted under this Agreement that Licensee
 has created using the Licensed Software. Under no circumstances may any copies
 of Redistributables be distributed separately.
 Right to distribute the Redistributables as part of the Application is
 conditional upon the Licensee not having any unpaid license fees owed to
 Licensor at the time of distribution of any Redistributables.
 16.4. Additional Restrictions
 The licenses granted under this Agreement are conditional and subject to
 Licensee's compliance with the following terms:
 * Application must add primary and substantial functionality to the
   Licensed Software.
 * Application must not compete with Licensed Software or services rendered
   by Licensor. In particular, Licensee may not create plug-ins, add-ons and
   alike for third-party software, which enable data import/export or other
   functionality provided by Licensed Software, without prior written
   Licensors consent.
 * Application may not pass on functionality which in any way makes it
   possible for others to create software that uses Licensed Software. In
   particular, Licensee may not create SDK (Software Development Kits),
   APIs (Application Programming Interfaces), software libraries and/or
   other development tools that provide access to functionality provided by
   Licensed Software.
